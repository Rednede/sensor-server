package master;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import javax.servlet.ServletContext;



public class Log {
	
	private String path = System.getProperty("catalina.base") + "/webapps/SensorMaster/log.txt";
	//public static final String path = System.getProperty("wtp.deploy") +"/SensorMaster/log.txt";
	
	//public static final String path= "C:/Users/Lourdes/workspace/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/SensorMaster/log.txt";
	public Log(){
		
		/*boolean fileExists = new File(path +"/SensorMaster/log.txt").isFile();
        
        if(!fileExists){
            f = new File(path +"/SensorMaster/log.txt");
        }
        f.path*/
	}
	
	public void logEntry(String user, String ip, String operacion, String elemento){
		FileWriter fw = null;
		BufferedWriter bw = null;
		PrintWriter out = null;
		try{
			
			File f = new File(path);
			if(!f.exists()){
				f.createNewFile();
			}	
			fw = new FileWriter(path, true);
			bw = new BufferedWriter(fw);
			
			 SimpleDateFormat dateFormatUtc = new SimpleDateFormat("yyyy-MMM-dd HH:mm:ss");
	         dateFormatUtc.setTimeZone(TimeZone.getTimeZone("UTC"));
	         String Fecha = dateFormatUtc.format(new Date());
	         out = new PrintWriter(bw);              
             out.println("\rLog Entry : ");
             out.println(Fecha );
             out.println("El usuario: " + user+ " con IP: "+ ip + " ha realizado la operacion " + operacion + " en " + elemento);
             out.println("--------------------------------------------------------------------------------------------------------");
		}catch(IOException e)   {
			e.printStackTrace();
		}finally{
			try{
				if (out != null){
					out.flush();
					out.close();
				}
					
				
				if (bw != null)
					bw.close();
	
				if (fw != null)
					fw.close();
			}catch (IOException ex){
				ex.printStackTrace();
			}
			
		}

	}	

}
