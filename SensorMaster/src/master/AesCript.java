package master;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import com.sun.org.apache.xml.internal.security.utils.Base64;

public class AesCript {

	public AesCript(){}
	
	
	public   String addPathToFile(String file){
    	
		
		
    	String path =System.getProperty("catalina.base");
        return  path+ "/webapps/SensorMaster/" +file;
		//return "C:/Users/Lourdes/workspace/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/SensorMaster/" + file;
    }
	
	 public  String readFile(String fileName) throws IOException {
	    BufferedReader br = new BufferedReader(new FileReader(fileName));
	    try {
	        StringBuilder sb = new StringBuilder();
	        String line = br.readLine();

	        while (line != null) {
	            sb.append(line);
	            line = br.readLine();
	        }
	        return sb.toString();
	    } finally {
	        br.close();
	    }
	}
	
	public   String encrypt( String value) {
        try {
        	String key = readFile(addPathToFile("key.txt")); // 128 bit key
        	
            String initVector = readFile(addPathToFile("iv.txt")); // 16 bytes IV
            
          
            IvParameterSpec iv = new IvParameterSpec(Base64.decode(initVector));
            
            SecretKeySpec skeySpec = new SecretKeySpec(Base64.decode(key), "AES");
            System.out.println(skeySpec);
            
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);
            /*
            byte[]   bytesEncoded = Base64.encodeBase64(value .getBytes());
            System.out.println("ecncoded value is " + new String(bytesEncoded ));
            return new String(bytesEncoded);

            */
            byte[] encrypted = cipher.doFinal(value.getBytes());
            System.out.println("encrypted string: "
                    + Base64.encode(encrypted));

            String result = new String(Base64.encode(encrypted));
            return result;
            //return Base64.encodeBase64(encrypted);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }
	
	
	  public    String decrypt( String encrypted) {
	        try {
	        	String key = readFile(addPathToFile("key.txt")); // 128 bit key
	            String initVector = readFile(addPathToFile("iv.txt")); // 16 bytes IV
	            
	            IvParameterSpec iv = new IvParameterSpec(Base64.decode(initVector));
	            
	            SecretKeySpec skeySpec = new SecretKeySpec(Base64.decode(key), "AES");
	        	

	            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
	            cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);

	            
	            //cambiar string to byte
	            byte[] b = encrypted.getBytes();
	            byte[] original = cipher.doFinal(Base64.decode(b));
	           //byte[] original = cipher.doFinal(Base64.decode(encrypted));
	            
	           
	            
	            return new String(original);
	        } catch (Exception ex) {
	            ex.printStackTrace();
	        }

	        return null;
	    }

	
	
}
