package master;
import java.io.*;
import java.util.*;
import java.text.*;
public class Sonda {
	
	  private String Volumen = "0";
	    private String Led = "0";
	    private String UltimaFecha = getFecha();
	    
	    public void crearSonda(){
	        //comprobamos si los files para esa cantidad existe, sino los creamos
	       

            boolean fileExists = new File(addPathToFile("sonda.txt")).isFile();
            
            if(!fileExists){
                File f = new File(addPathToFile("sonda.txt"));
                try{
                    f.createNewFile();

                    FileWriter fw = new FileWriter(f);
                    BufferedWriter out = new BufferedWriter(fw);
                    	
                        out.write("Volumen="+Volumen);
                        out.newLine();
                        out.write("UltimaFecha="+UltimaFecha);
                        out.newLine();
                        out.write("Led="+Led);
                        out.newLine();

                    out.flush();
                out.close();
                    
                }catch(Exception e){
                    System.err.println("error creando archivo sonda.txt: " + e);
                }

	        }
	    
	    }
	    
	    public String addPathToFile(String file){
	    	
	    	//String path =System.getProperty("wtp.deploy");
	    	String path =System.getProperty("catalina.base");
	        return  path+ "/webapps/SensorMaster/" +file+ ".txt";
	    	//return "C:/Users/Lourdes/workspace/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/SensorMaster/" + file + ".txt";
	    }
	    public String concreteLineFromFile(int line, String file){
	        
	        try{
	            // Open the file
	            FileInputStream fstream = new FileInputStream(file);
	            BufferedReader br = new BufferedReader(new InputStreamReader(fstream));

	            String strLine;
	            int pos = 0;
	            
	            while ((strLine = br.readLine()) != null)   {
	                
	                if(pos == line){
	                	br.close();
	                	return strLine;
	                }
	                
	                pos ++;
	            }

	            //Close the input stream
	            br.close();
	            
	        }catch(Exception e){
	        System.err.println("Error leyendo fichero: " + e);
	        }
	        return "";
	    }
	    public String cogerDatoDeLinea(String line){
	        String s = "";
	        try{
	            StringTokenizer st = new StringTokenizer(line, "=");
	        
	       
	            s = st.nextToken();
	       
	            s = st.nextToken();
	            return s;
	        }catch(Exception e){
	        System.err.println("Se ha producido el error al coger el dato de linea: " + e);
	        }
	        return s;
	    }
	    public String getFecha(){
	    	Date dNow = new Date();
	        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
	        String Fecha = ft.format(dNow);
	        
	        return Fecha;
	    }

	    /**
	     * Devuelve la fecha actual
	     * @return 
	     */

	    public String fecha(String user, String ip, String operacion, String elemento){
	    	AesCript c = new AesCript();
	    	user = c.decrypt(user);
	    	ip = c.decrypt(ip);
	    	operacion = c.decrypt(operacion);
	    	elemento = c.decrypt(elemento);
	        String Fecha = getFecha();
	        Log log = new Log();
	        log.logEntry(user, ip, operacion, elemento);
	        c.encrypt(Fecha);
	        return Fecha;
	    }
	    

	    public String getVolumen(String user, String ip, String operacion, String elemento ) {
	    	AesCript c = new AesCript();
	    	user = c.decrypt(user);
	    	ip = c.decrypt(ip);
	    	operacion = c.decrypt(operacion);
	    	elemento = c.decrypt(elemento);
	    	
	        String volume = concreteLineFromFile( 0,  addPathToFile("sonda.txt"));
	        volume = cogerDatoDeLinea(volume);
	        Log log = new Log();
	        log.logEntry(user, ip, operacion, elemento);
	        c.encrypt(volume);
	        return volume;
	    }

	    public String getLed(String user, String ip, String operacion, String elemento) {
	    	AesCript c = new AesCript();
	    	user = c.decrypt(user);
	    	ip = c.decrypt(ip);
	    	operacion = c.decrypt(operacion);
	    	elemento = c.decrypt(elemento);
	    	
	        String led = concreteLineFromFile( 2,  addPathToFile("sonda.txt"));
	        led = cogerDatoDeLinea(led);
	        Log log = new Log();
	        log.logEntry(user, ip, operacion, elemento);
	        c.encrypt(led);
	        return led;
	    }


	    public String getUltimaFecha(String user, String ip, String operacion, String elemento) {
	    	AesCript c = new AesCript();
	    	user = c.decrypt(user);
	    	ip = c.decrypt(ip);
	    	operacion = c.decrypt(operacion);
	    	elemento = c.decrypt(elemento);
	    	
	    	String fecha = concreteLineFromFile( 1,  addPathToFile("sonda.txt"));
	        fecha = cogerDatoDeLinea(fecha);
	        Log log = new Log();
	        log.logEntry(user, ip, operacion, elemento);
	        c.encrypt(fecha);
	        return fecha;
	    }

	    public void setLed(String Led, String user, String ip, String operacion, String elemento) {
	    	/*AesCript c = new AesCript();
	    	user = c.decrypt(user);
	    	ip = c.decrypt(ip);
	    	operacion = c.decrypt(operacion);
	    	elemento = c.decrypt(elemento);*/
	    	
	        List<String> lines = new ArrayList<String>();
	        String line = null;
	        try {
	            File f1 = new File(addPathToFile("sonda.txt"));
	            FileReader fr = new FileReader(f1);
	            BufferedReader br = new BufferedReader(fr);
	        while ((line = br.readLine()) != null) {
	            if (line.contains("Led")){
	                line = line.replace(line, "Led=" + Led );
	            }
	                
	            lines.add(line);
	        }
	        fr.close();
	        br.close();

	        FileWriter fw = new FileWriter(f1);
	        BufferedWriter out = new BufferedWriter(fw);
	        for(String s : lines){
	            out.write(s+"\n");
	        }
	             
	        out.flush();
	        out.close();
	        Log log = new Log();
	        log.logEntry(user, ip, operacion, elemento);
	        
	   } 
	        
	   catch (Exception ex) {
	        ex.printStackTrace();
	    }
	        
	        
	        
	    }

	    
	    public boolean comprobarVolumenCorrecto(double vol)
	    {
	    return (vol >= 0 && vol <= 100);
	    }
	    public boolean comprobarLedCorrecto(double led)
	    {
	    return (led >= 0 && led <= 65535);
	    }
	    

	    public String cambiarLed(String Led, String user, String ip, String operacion, String elemento){
	    	AesCript c = new AesCript();
	    	Led =c.decrypt(Led);
	    	user = c.decrypt(user);
	    	ip = c.decrypt(ip);
	    	operacion = c.decrypt(operacion);
	    	elemento = c.decrypt(elemento);
	        if(comprobarLedCorrecto(Double.parseDouble(Led))){
	            setLed(Led, user, ip, operacion, elemento);
	            return c.encrypt("Se ha cambiado el LED a " + Led);
	            
	        }
	        else{
	        return "El valor introducido no es correcto";
	        }
	    
	    }

}
